﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Net.Http;

namespace OwlBotAPI
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async void Handle_GetDefinition(object sender, System.EventArgs e)
        {
                var client = new HttpClient();
                var owlBotAddress = "https://owlbot.info/api/v2/dictionary/<word>";
                var uri = new Uri(owlBotAddress);

                OwlBotModel definitionData = new OwlBotModel();
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    definitionData = JsonConvert.DeserializeObject<OwlBotModel>(jsonContent);

                }

            defListView.ItemsSource = new ObservableCollection<owlDef>(definitionData.Dictionary);
        }

        }
    }
