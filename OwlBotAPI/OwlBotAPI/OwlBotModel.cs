﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace OwlBotAPI
{

    public class owlDef
    {
        
        [JsonProperty(PropertyName ="type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName ="definition")]
        public string Definition { get; set; }
        [JsonProperty(PropertyName ="example")]
        public string Example { get; set; }
        
    }
    public class OwlBotModel
    {
        public List<owlDef> Dictionary { get; set; }
       

    }
}
